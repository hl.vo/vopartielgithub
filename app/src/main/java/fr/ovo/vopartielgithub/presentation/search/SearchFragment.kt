package fr.ovo.vopartielgithub.presentation.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.ovo.vopartielgithub.R
import fr.ovo.vopartielgithub.presentation.MainActivity
import fr.ovo.vopartielgithub.presentation.UserShortAdapter

class SearchFragment : Fragment(), UserShortAdapter.OnUserClickListener
{
    companion object {
        fun newInstance() = SearchFragment()
    }

    private lateinit var editText: EditText
    private lateinit var button: Button
    private lateinit var recyclerView: RecyclerView
    private lateinit var progress: ProgressBar

    private lateinit var adapter: UserShortAdapter

    private val viewModel: SearchViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search_user, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        editText = view.findViewById(R.id.fragment_search_edittext)
        button = view.findViewById(R.id.fragment_search_button)
        progress = view.findViewById(R.id.fragment_search_progress)
        recyclerView = view.findViewById(R.id.fragment_search_recyclerview)

        adapter = UserShortAdapter(requireContext(), this)

        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerView.adapter = adapter

        button.setOnClickListener {
            viewModel.searchUser(editText.text.toString())
        }

        viewModel.state.observe(viewLifecycleOwner, ::onStateChanged)
    }

    private fun onStateChanged(state: SearchState) {
        when (state) {
            is SearchState.Loading -> {

                progress.visibility = View.VISIBLE

                adapter.setUsers(null)
            }
            is SearchState.Success -> {

                progress.visibility = View.GONE

                adapter.setUsers(state.userShort)
            }
            is SearchState.Error -> {
                progress.visibility = View.GONE

                adapter.setUsers(null)

                Toast.makeText(activity, "Error", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onUserClicked(name: String) {
        (activity as MainActivity).displayUserDetail(name)
    }


}