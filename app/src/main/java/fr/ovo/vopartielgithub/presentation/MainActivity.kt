package fr.ovo.vopartielgithub.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.FragmentContainerView
import androidx.fragment.app.commit
import fr.ovo.vopartielgithub.R
import fr.ovo.vopartielgithub.presentation.detail.DetailFragment
import fr.ovo.vopartielgithub.presentation.search.SearchFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            supportFragmentManager.commit {
                add(R.id.activity_main_container_1, SearchFragment.newInstance())
            }
        }
    }

    fun displayUserDetail(id: String) {
        if (findViewById<FragmentContainerView>(R.id.activity_main_container_2) != null) {
            supportFragmentManager.commit {
                add(R.id.activity_main_container_2, DetailFragment.newInstance(id))
            }
        } else {
            supportFragmentManager.commit {
                replace(R.id.activity_main_container_1, DetailFragment.newInstance(id))
                addToBackStack(null)
            }
        }
    }
}