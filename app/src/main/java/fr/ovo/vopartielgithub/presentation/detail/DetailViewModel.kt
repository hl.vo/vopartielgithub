package fr.ovo.vopartielgithub.presentation.detail

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import fr.ovo.vopartielgithub.data.repository.GithubUserRepository
import fr.ovo.vopartielgithub.domain.model.UserDetail
import fr.ovo.vopartielgithub.domain.repository.UserRepository
import kotlinx.coroutines.launch

class DetailViewModel(application: Application) : AndroidViewModel(application)
{
    private val userRepository:UserRepository = GithubUserRepository()

    private val _data = MutableLiveData<UserDetail>()
    val data: LiveData<UserDetail> get() = _data

    fun getUserDetail(userId: String) {
        viewModelScope.launch {
            _data.value = userRepository.getUserDetail(userId)
        }
    }
}