package fr.ovo.vopartielgithub.presentation.search

import fr.ovo.vopartielgithub.domain.model.UserShort

sealed class SearchState {
    class Success(val userShort: List<UserShort>) : SearchState()

    object Loading : SearchState()

    object Error : SearchState()
}