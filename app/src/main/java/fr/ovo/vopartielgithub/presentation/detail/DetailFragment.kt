package fr.ovo.vopartielgithub.presentation.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.squareup.picasso.Picasso
import fr.ovo.vopartielgithub.R
import fr.ovo.vopartielgithub.domain.model.UserDetail

class DetailFragment : Fragment() {

    companion object {
        private const val KEY_ID = "key_id"

        fun newInstance(id: String): DetailFragment {
            val bundle = Bundle()
            bundle.putString(KEY_ID, id)

            val fragment = DetailFragment()
            fragment.arguments = bundle

            return fragment
        }
    }

    private lateinit var name: TextView
    private lateinit var description: TextView
    private lateinit var forks: TextView
    private lateinit var language: TextView
    private lateinit var watchers: TextView
    private lateinit var license: TextView


    private val viewModel: DetailViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_details_user, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        name = view.findViewById(R.id.fragment_detail_name)
        description = view.findViewById(R.id.fragment_detail_description)
        forks = view.findViewById(R.id.fragment_detail_forks)
        language = view.findViewById(R.id.fragment_detail_language)
        watchers = view.findViewById(R.id.fragment_detail_watchers)
        license = view.findViewById(R.id.fragment_detail_license)

        viewModel.data.observe(viewLifecycleOwner, ::onStateChanged)

        arguments?.getString(KEY_ID)?.let {
            viewModel.getUserDetail(it)
        }
    }

    private fun onStateChanged(userDetail: UserDetail) {

        name.text = userDetail.name
        description.text = userDetail.description
        forks.text = userDetail.forks.toString()
        watchers.text = userDetail.watchers.toString()
        language.text = userDetail.language
        license.text = userDetail.license.name
        //license.text = userDetail.license.javaClass.getField("name").toString()
    }
}