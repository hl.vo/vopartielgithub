package fr.ovo.vopartielgithub.data.api.model

import com.google.gson.annotations.SerializedName

data class GithubSearchResponse(
    @SerializedName("items")
    val users: List<GithubUserShort>,

    @SerializedName("total_count")
    val usersNumber: Int,

    @SerializedName("incomplete_results")
    val response: Boolean
)