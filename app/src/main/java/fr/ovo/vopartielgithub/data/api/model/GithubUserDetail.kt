package fr.ovo.vopartielgithub.data.api.model

import com.google.gson.annotations.SerializedName
import fr.ovo.vopartielgithub.domain.model.License

data class GithubUserDetail(
    val name: String,

    val description : String?,

    val language : String?,

    val forks : Int,

    @SerializedName("watchers")
    val watchers : Int,

    @SerializedName("license")
    val license : License
)


/*
id	460600860
node_id	"R_kgDOG3Q2HA"
name	".allstar"
full_name	"google/.allstar"
private	false
owner	{…}
html_url	"https://github.com/google/.allstar"
description	null
fork	false
url	"https://api.github.com/repos/google/.allstar"
forks_url	"https://api.github.com/repos/google/.allstar/forks"
keys_url	"https://api.github.com/r…e/.allstar/keys{/key_id}"
collaborators_url	"https://api.github.com/r…aborators{/collaborator}"
teams_url	"https://api.github.com/repos/google/.allstar/teams"
hooks_url	"https://api.github.com/repos/google/.allstar/hooks"
issue_events_url	"https://api.github.com/r…r/issues/events{/number}"
events_url	"https://api.github.com/r…s/google/.allstar/events"
assignees_url	"https://api.github.com/r…allstar/assignees{/user}"
branches_url	"https://api.github.com/r…llstar/branches{/branch}"
tags_url	"https://api.github.com/repos/google/.allstar/tags"
blobs_url	"https://api.github.com/r….allstar/git/blobs{/sha}"
git_tags_url	"https://api.github.com/r…/.allstar/git/tags{/sha}"
git_refs_url	"https://api.github.com/r…/.allstar/git/refs{/sha}"
trees_url	"https://api.github.com/r….allstar/git/trees{/sha}"
statuses_url	"https://api.github.com/r…/.allstar/statuses/{sha}"
languages_url	"https://api.github.com/r…oogle/.allstar/languages"
stargazers_url	"https://api.github.com/r…ogle/.allstar/stargazers"
contributors_url	"https://api.github.com/r…le/.allstar/contributors"
subscribers_url	"https://api.github.com/r…gle/.allstar/subscribers"
subscription_url	"https://api.github.com/r…le/.allstar/subscription"
commits_url	"https://api.github.com/r…e/.allstar/commits{/sha}"
git_commits_url	"https://api.github.com/r…llstar/git/commits{/sha}"
comments_url	"https://api.github.com/r…llstar/comments{/number}"
issue_comment_url	"https://api.github.com/r…issues/comments{/number}"
contents_url	"https://api.github.com/r…allstar/contents/{+path}"
compare_url	"https://api.github.com/r…/compare/{base}...{head}"
merges_url	"https://api.github.com/r…s/google/.allstar/merges"
archive_url	"https://api.github.com/r…r/{archive_format}{/ref}"
downloads_url	"https://api.github.com/r…oogle/.allstar/downloads"
issues_url	"https://api.github.com/r….allstar/issues{/number}"
pulls_url	"https://api.github.com/r…/.allstar/pulls{/number}"
milestones_url	"https://api.github.com/r…star/milestones{/number}"
notifications_url	"https://api.github.com/r…since,all,participating}"
labels_url	"https://api.github.com/r…e/.allstar/labels{/name}"
releases_url	"https://api.github.com/r…e/.allstar/releases{/id}"
deployments_url	"https://api.github.com/r…gle/.allstar/deployments"
created_at	"2022-02-17T20:40:32Z"
updated_at	"2022-05-19T14:40:06Z"
pushed_at	"2022-05-31T18:39:22Z"
git_url	"git://github.com/google/.allstar.git"
ssh_url	"git@github.com:google/.allstar.git"
clone_url	"https://github.com/google/.allstar.git"
svn_url	"https://github.com/google/.allstar"
homepage	null
size	12
stargazers_count	3
watchers_count	3
language	null
has_issues	true
has_projects	true
has_downloads	true
has_wiki	true
has_pages	false
has_discussions	false
forks_count	0
mirror_url	null
archived	false
disabled	false
open_issues_count	0
license	{…}
allow_forking	true
is_template	false
web_commit_signoff_required	false
topics	[]
visibility	"public"
forks	0
open_issues	0
watchers	3
default_branch	"main"*/
