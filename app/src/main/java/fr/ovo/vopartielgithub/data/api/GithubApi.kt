package fr.ovo.vopartielgithub.data.api

import fr.ovo.vopartielgithub.data.api.model.GithubSearchResponse
import fr.ovo.vopartielgithub.data.api.model.GithubUserDetail
import fr.ovo.vopartielgithub.data.api.model.GithubUserShort
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GithubApi {
    companion object {
        const val BASE_URL = "https://api.github.com/"
    }

    @GET("/search/users")
    suspend fun searchUser(
        @Query("q") name : String,
    ): GithubSearchResponse

    @GET("/users/{name}/repos")
    suspend fun getUserDetail(
        @Path("name") name: String,
    ): List<GithubUserDetail>


}