package fr.ovo.vopartielgithub.data.api.model

import com.google.gson.annotations.SerializedName

data class GithubUserShort(
    val login : String,

    @SerializedName("avatar_url")
    val avatarUrl : String
)