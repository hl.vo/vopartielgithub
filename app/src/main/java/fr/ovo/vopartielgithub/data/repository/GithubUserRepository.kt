package fr.ovo.vopartielgithub.data.repository

import fr.ovo.vopartielgithub.data.api.GithubApi
import fr.ovo.vopartielgithub.data.api.model.GithubUserDetail
import fr.ovo.vopartielgithub.data.api.model.GithubUserShort
import fr.ovo.vopartielgithub.domain.model.UserDetail
import fr.ovo.vopartielgithub.domain.model.UserShort
import fr.ovo.vopartielgithub.domain.repository.UserRepository
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class GithubUserRepository : UserRepository {

    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(GithubApi.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    private val api = retrofit.create(GithubApi::class.java)


    override suspend fun searchUser(search: String): List<UserShort> {
       return api.searchUser(search).users.map {
           it.toUserShort()
       }
    }

    override suspend fun getUserDetail(userId: String): List<UserDetail> {
        return api.getUserDetail(userId).map {
            it.toUserDetail()
        }
    }

}

private fun GithubUserShort.toUserShort() =
    UserShort(
        this.login,
        this.avatarUrl
    )

private fun GithubUserDetail.toUserDetail() =
    UserDetail(
        this.name,
        this.description.toString(),
        this.language.toString(),
        this.forks,
        this.watchers,
        this.license
    )