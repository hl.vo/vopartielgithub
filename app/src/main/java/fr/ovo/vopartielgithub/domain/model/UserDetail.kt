package fr.ovo.vopartielgithub.domain.model

import com.google.gson.annotations.SerializedName

data class UserDetail (
    val name: String,
    val description : String?,
    val language : String?,
    val forks : Int,
    val watchers : Int,
    val license : License)