package fr.ovo.vopartielgithub.domain.repository

import fr.ovo.vopartielgithub.domain.model.UserDetail
import fr.ovo.vopartielgithub.domain.model.UserShort

interface UserRepository {
    suspend fun searchUser(search: String): List<UserShort>
    suspend fun getUserDetail(userId: String): List<UserDetail>
}