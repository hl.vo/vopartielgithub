package fr.ovo.vopartielgithub.domain.model

import com.google.gson.annotations.SerializedName

data class UserShort (
    val login : String,
    val avatarUrl : String
    )