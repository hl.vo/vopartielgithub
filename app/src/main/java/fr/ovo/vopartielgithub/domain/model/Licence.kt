package fr.ovo.vopartielgithub.domain.model

data class License(
    val name : String
)